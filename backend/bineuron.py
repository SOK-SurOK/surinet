import numpy as np
from typing import List, Union
import random
import numba as nb


@nb.njit(parallel=True, fastmath=True)
def find_count_equal(arr2: Union[np.ndarray, List[List[bool]]], arr1: Union[np.ndarray, List[bool]]) -> int:
    ret = np.zeros(arr2.shape[0], dtype=np.uint8)
    for i in nb.prange(arr2.shape[0]):
        if np.array_equal(arr2[i], arr1):
            ret[i] = 1
    return np.sum(ret)


@nb.njit(fastmath=True)
def find_any_equal(arr2: Union[np.ndarray, List[List[bool]]], arr1: Union[np.ndarray, List[bool]]) -> bool:
    for i in range(arr2.shape[0]):
        if np.array_equal(arr2[i], arr1):
            return True
    return False


def avg_not_zero(arr: Union[np.ndarray, List[float]]) -> float:
    cond = arr[arr > 0]
    if len(cond) != 0:
        return np.average(cond)
    else:
        return 0


def min_not_zero(arr: Union[np.ndarray, List[float]]) -> float:
    cond = arr[arr > 0]
    if len(cond) != 0:
        return np.min(cond)
    else:
        return 0


class BiNeuron:
    cluster_weight: Union[np.ndarray, List[List[float]]]
    layer_cluster: Union[np.ndarray, List[List[bool]]]

    anti_cluster_weight: Union[np.ndarray, List[List[float]]]
    layer_anti_cluster: Union[np.ndarray, List[List[bool]]]

    threshold: int = 1

    def __init__(self, size_in_vector: int, count_cluster: int):
        self.layer_cluster = np.random.randint(0, 2, (count_cluster, size_in_vector), dtype=np.bool_)
        self.cluster_weight = np.zeros(self.layer_cluster.shape)
        self.layer_anti_cluster = np.random.randint(0, 2, (count_cluster, size_in_vector), dtype=np.bool_)
        self.anti_cluster_weight = np.zeros(self.layer_anti_cluster.shape)

    def forward(self, in_vector: Union[np.ndarray, List[bool]]) -> bool:
        sum_neuron = 0
        for i in range(self.layer_cluster.shape[0]):
            sum_and_i = np.sum(in_vector & self.layer_cluster[i])
            sum_i = np.sum(self.layer_cluster[i])
            sum_and_anti_i = np.sum(in_vector & self.layer_anti_cluster[i])
            sum_anti_i = np.sum(self.layer_anti_cluster[i])
            if sum_and_i == sum_i and (sum_and_anti_i != sum_anti_i or np.sum(self.anti_cluster_weight) == 0):
                sum_neuron += sum_i
        return sum_neuron >= self.threshold

    def lesson_del_add(self, good_in_vector: Union[np.ndarray, List[bool]]):
        for c in range(len(self.layer_cluster)):
            new_vector = good_in_vector & np.random.choice([True, False], len(good_in_vector), p=[0.1, 0.9])
            if np.sum(self.layer_cluster[c]) <= 1 or \
                    (np.sum(self.cluster_weight[c]) == 0 and random.getrandbits(1) and not find_any_equal(
                        self.layer_cluster, new_vector)):
                self.layer_cluster[c] = new_vector
                self.cluster_weight[c] = np.ones(self.cluster_weight.shape[1]) * 0.5
                self.anti_cluster_weight[c] *= 0
                break

    def lesson_weight(self, good_in_vector):
        for c in range(self.layer_cluster.shape[0]):
            vector_and_i = good_in_vector & self.layer_cluster[c]
            sum_and_i = np.sum(vector_and_i)
            self.cluster_weight[c] += vector_and_i * sum_and_i
            maxi = np.max(self.cluster_weight[c])
            if maxi == 0:
                self.cluster_weight[c] *= 0
            else:
                self.cluster_weight[c] /= maxi

    def lesson_cut(self):
        for i in range(self.layer_cluster.shape[0]):
            avg_nz = avg_not_zero(self.cluster_weight[i])
            for j in range(self.layer_cluster.shape[1]):
                if self.cluster_weight[i][j] < avg_nz:
                    self.layer_cluster[i][j] = False
                    self.cluster_weight[i][j] = 0
                if np.sum(self.layer_cluster[i]) == 0:
                    self.cluster_weight[i] *= 0
                    continue
            if find_count_equal(self.layer_cluster, self.layer_cluster[i]) > 1:
                self.cluster_weight[i] *= 0

    def lesson_check_bad(self,
                         arr_in_vector: Union[np.ndarray, List[List[bool]]],
                         arr_want: Union[np.ndarray, List[bool]]):
        good1 = np.zeros(self.layer_cluster.shape[0])
        good0 = np.zeros(self.layer_cluster.shape[0])
        bad = np.zeros(self.layer_cluster.shape[0])
        bad_alt = np.zeros(self.layer_cluster.shape[0])
        for t in range(len(arr_want)):
            for c in range(self.layer_cluster.shape[0]):
                sum_and_i = np.sum(arr_in_vector[t] & self.layer_cluster[c])
                sum_i = np.sum(self.layer_cluster[c])
                work = sum_and_i == sum_i
                if arr_want[t] and work:
                    good1[c] += 1
                if not arr_want[t] and not work:
                    good0[c] += 1
                if not arr_want[t] and work:
                    bad[c] += 1
                    if np.sum(self.anti_cluster_weight) == 0:
                        self.layer_anti_cluster[c] = arr_in_vector[t] & np.random.choice(
                            [True, False],
                            self.layer_anti_cluster.shape[1],
                            p=[0.1, 0.9]
                        )
                        self.anti_cluster_weight[c] = self.anti_cluster_weight[c] * 0 + 0.5
                    else:
                        anti_vector_and_i = arr_in_vector[t] & self.layer_anti_cluster[c]
                        anti_sum_and_i = np.sum(anti_vector_and_i)
                        self.anti_cluster_weight[c] += anti_vector_and_i * anti_sum_and_i
                        maxi = np.max(self.anti_cluster_weight[c])
                        if maxi == 0:
                            self.anti_cluster_weight[c] *= 0
                        else:
                            self.anti_cluster_weight[c] /= maxi
                if arr_want[t] and not work:
                    bad_alt[c] += 1
        relation = (bad + bad_alt) / (good0 + good1 + bad + bad_alt) * 100
        max_relation = np.max(relation)
        min_relation = np.min(relation)
        avg_relation = np.average(relation)
        if round(min_relation, 1) != round(max_relation, 1):
            for i in range(self.layer_cluster.shape[0]):
                if relation[i] >= max_relation:
                    self.cluster_weight[i] *= 0
        else:
            if avg_relation >= 50:
                count = 0
                max_count = self.layer_cluster.shape[0] * 0.1
                for i in range(self.layer_cluster.shape[0]):
                    if count >= max_count:
                        break
                    if random.getrandbits(1):
                        self.cluster_weight[i] *= 0
                        count += 1
        for i in range(self.layer_cluster.shape[0]):
            if np.sum(self.cluster_weight[i]) != 0:
                avg_nz = avg_not_zero(self.anti_cluster_weight[i])
                for j in range(self.layer_anti_cluster.shape[1]):
                    if self.anti_cluster_weight[i][j] < avg_nz:
                        self.layer_anti_cluster[i][j] = False
                        self.anti_cluster_weight[i][j] = 0
                    if np.sum(self.layer_anti_cluster[i]) <= 1:
                        self.anti_cluster_weight[i] *= 0
                        continue
                if find_count_equal(self.layer_anti_cluster, self.layer_anti_cluster[i]) > 1:
                    self.anti_cluster_weight[i] *= 0

    def lesson_threshold(self,
                         arr_in_vector: Union[np.ndarray, List[List[bool]]],
                         arr_want: Union[np.ndarray, List[bool]]):
        l0, l1 = [], []
        for t in range(len(arr_want)):
            sum_neuron = 0
            for i in range(self.layer_cluster.shape[0]):
                sum_and_i = np.sum(arr_in_vector[t] & self.layer_cluster[i])
                sum_i = np.sum(self.layer_cluster[i])
                sum_and_anti_i = np.sum(arr_in_vector[t] & self.layer_anti_cluster[i])
                sum_anti_i = np.sum(self.layer_anti_cluster[i])
                if sum_and_i == sum_i and (sum_and_anti_i != sum_anti_i or np.sum(self.anti_cluster_weight) == 0):
                    sum_neuron += sum_i
            if arr_want[t]:
                l1.append(sum_neuron)
            else:
                l0.append(sum_neuron)
        self.threshold = (np.max(l0) + np.min(l1)) * 0.5 + 0.1  # ноль не желательно
