import numpy as np
from typing import List, Union
from backend.bineuron import BiNeuron
import random


class BiNet:
    layer_neuron: List[BiNeuron]
    hash_1d: List[np.ndarray] = []
    hash_2d: List[np.ndarray] = []

    def __init__(self, size_in_vector: int, count_cluster: int, count_neuron: int,
                 hash_window: int, hash_1d_count: int, hash_2d_count: int):
        self.size_in_vector = size_in_vector
        self.count_neuron = count_neuron
        self.rnd_id = random.randint(0, 1024)

        self.hash_1d = [np.random.randint(0, 2, hash_window, dtype=np.bool_) for i in
                        range(hash_1d_count)]
        self.hash_2d = [np.random.randint(0, 2, (hash_window, hash_window), dtype=np.bool_) for i in
                        range(hash_2d_count)]

        self.layer_neuron = [BiNeuron(size_in_vector, count_cluster) for i in range(count_neuron)]

    def forward(self, in_vector: np.ndarray) -> np.ndarray:
        out_arr = []
        for i in range(self.count_neuron):
            out_arr.append(self.layer_neuron[i].forward(in_vector))
        return np.array(out_arr)

    def lesson_del_add(self, arr_in_vector: Union[List[np.ndarray], List[List[bool]]],
                       arr_want: Union[List[np.ndarray], List[List[bool]]]):
        for i in range(len(arr_in_vector)):
            for n in range(self.count_neuron):
                if arr_want[i][n]:
                    self.layer_neuron[n].lesson_del_add(arr_in_vector[i])

    def lesson_weight(self, arr_in_vector: Union[List[np.ndarray], List[List[bool]]],
                      arr_want: Union[List[np.ndarray], List[List[bool]]]):
        for i in range(len(arr_in_vector)):
            for n in range(self.count_neuron):
                if arr_want[i][n]:
                    self.layer_neuron[n].lesson_weight(arr_in_vector[i])

    def lesson_cut(self):
        for n in range(self.count_neuron):
            self.layer_neuron[n].lesson_cut()

    def lesson_check_bad(self, arr_in_vector: Union[List[np.ndarray], List[List[bool]]],
                         arr_want: Union[List[np.ndarray], List[List[bool]]]):
        np_want = np.array(arr_want)
        for n in range(self.count_neuron):
            self.layer_neuron[n].lesson_check_bad(arr_in_vector, np_want[:, n])

    def lesson_threshold(self, arr_in_vector: Union[List[np.ndarray], List[List[bool]]],
                         arr_want: Union[List[np.ndarray], List[List[bool]]]):
        np_want = np.array(arr_want)
        for n in range(self.count_neuron):
            self.layer_neuron[n].lesson_threshold(arr_in_vector, np_want[:, n])

    @staticmethod
    def out_hash_1d_func(in_values: Union[np.ndarray, List[bool]], size):
        vector = np.zeros(size, dtype=np.bool_)
        t = 0
        for xx in range(in_values.shape[0]):
            vector[t % size] = in_values[xx]
            t += 1
        return vector

    def hash_1d_func(self, in_values: Union[np.ndarray, List[bool]], size):
        vector = np.zeros(size, dtype=np.bool_)
        x = self.hash_1d[0].shape[0]
        for xx in range(in_values.shape[0] // x):
            value = in_values[xx * x:(xx + 1) * x]
            hash_id = (xx ^ self.rnd_id) % len(self.hash_1d)
            vector[xx % vector.shape[0]] = np.sum(self.hash_1d[hash_id] & value) == np.sum(self.hash_1d[hash_id])
        return vector

    @staticmethod
    def hash_2d_func(in_values: Union[np.ndarray, List[List[bool]]], size):
        vector = np.zeros(size, dtype=np.bool_)
        t = 0
        for yy in range(in_values.shape[0]):
            for xx in range(in_values.shape[1]):
                vector[t % size] = in_values[yy][xx]
                t += 1
        return vector

    def dump_neuron_cluster(self, i: int):
        return self.layer_neuron[i].layer_cluster

    def dump_cluster_weight(self, i: int):
        return self.layer_neuron[i].cluster_weight

    def dump_neuron_anti_cluster(self, i: int):
        return self.layer_neuron[i].layer_anti_cluster

    def dump_neuron_weight_anti_cluster(self, i: int):
        return self.layer_neuron[i].anti_cluster_weight
