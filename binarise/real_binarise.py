import cv2 as cv
import os
import numpy as np


def main():
    directory = 'real/'
    files = os.listdir(directory)
    save_arr0 = np.zeros((115, 230, 310), dtype=np.bool_)
    save_arr1 = np.zeros((115, 35, 47), dtype=np.bool_)
    save_arr2 = np.zeros((115, 69, 93), dtype=np.bool_)
    save_arr3 = np.zeros((115, 23, 31), dtype=np.bool_)
    save_arr4 = np.zeros((115, 115, 155), dtype=np.bool_)
    for file in files:
        if file.endswith("_0_tlp.bmp"):
            img = cv.imread(directory + file, 0)  # 0 = grayscale
            index = int(file[:len(file) - 10]) - 1
            save_arr0[index] = img
        if file.endswith("_1_tlp.bmp"):
            img = cv.imread(directory + file, 0)  # 0 = grayscale
            index = int(file[:len(file) - 10]) - 1
            th_resized = cv.resize(img, (47, 35))
            ret, th_th = cv.threshold(th_resized, 1, 255, cv.THRESH_BINARY)
            save_arr1[index] = th_th
        if file.endswith("_2_tlp.bmp"):
            img = cv.imread(directory + file, 0)  # 0 = grayscale
            index = int(file[:len(file) - 10]) - 1
            th_resized = cv.resize(img, (int(31*3), int(23*3)))
            ret, th_th = cv.threshold(th_resized, 1, 255, cv.THRESH_BINARY)
            save_arr2[index] = th_th
        if file.endswith("_3_tlp.bmp"):
            img = cv.imread(directory + file, 0)  # 0 = grayscale
            index = int(file[:len(file) - 10]) - 1
            th_resized = cv.resize(img, (int(31 * 1), int(23 * 1)))
            ret, th_th = cv.threshold(th_resized, 1, 255, cv.THRESH_BINARY)
            save_arr3[index] = th_th
        if file.endswith("_4_tlp.bmp"):
            img = cv.imread(directory + file, 0)  # 0 = grayscale
            index = int(file[:len(file) - 10]) - 1
            th_resized = cv.resize(img, (int(31 * 5), int(23 * 5)))
            ret, th_th = cv.threshold(th_resized, 1, 255, cv.THRESH_BINARY)
            save_arr4[index] = th_th

    np.save('save_arr0', save_arr0)
    np.save('save_arr1', save_arr1)
    np.save('save_arr2', save_arr2)
    np.save('save_arr3', save_arr3)
    np.save('save_arr4', save_arr4)


if __name__ == "__main__":
    main()
