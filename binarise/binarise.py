import cv2 as cv
import os
import numpy as np


def main():
    directory = 'photos/'
    files = os.listdir(directory)
    print(files)
    save_arr = []
    for file in files:
        img = cv.imread(directory + file, 0)

        blur = cv.medianBlur(img, 5)
        th_original = cv.adaptiveThreshold(blur, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY, 11, 2)
        th_resized = cv.resize(th_original, (93, 69))
        ret, th_th = cv.threshold(th_resized, 254, 255, cv.THRESH_BINARY)

        save_arr.append(th_th)

    save_arr = np.array(save_arr, dtype=np.bool_)
    print(save_arr.shape)
    save_arr = ~save_arr
    np.save('peoples2', save_arr)


if __name__ == "__main__":
    main()
