from tk_base import BaseTK

if __name__ == '__main__':
    app = BaseTK()
    app.update_idletasks()
    x = int((app.winfo_screenwidth() - app.winfo_reqwidth()) * 0.5)
    y = int((app.winfo_screenheight() - app.winfo_reqheight()) * 0.5)
    app.geometry("+{:d}+{:d}".format(x, y))
    app.mainloop()
