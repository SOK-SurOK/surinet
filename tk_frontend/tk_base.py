import tkinter as tk
import tkinter.messagebox as mb
from pages.tk_page_start import StartPage
from pages.tk_page_binet import PageBiNet
from pages.tk_page_new_net import PageNewNet
from pages.tk_page_new_examples import PageNewExamples


class BaseTK(tk.Tk):
    id_current_frame: int
    current_radio: tk.IntVar

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        tk.Tk.wm_title(self, "Киберсеть")

        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        menubar = tk.Menu(container)

        filemenu = tk.Menu(menubar, tearoff=0)
        filemenu.add_command(label="warning", command=lambda: mb.showwarning("Warning", "warning!"))
        filemenu.add_command(label="info", command=lambda: mb.showinfo("Info", "info!"))
        filemenu.add_command(label="error", command=lambda: mb.showerror("Error", "error!"))
        filemenu.add_separator()
        filemenu.add_command(label="Выход", command=quit)
        menubar.add_cascade(label="File", menu=filemenu)

        select_frame = tk.Menu(menubar, tearoff=0)
        self.current_radio = tk.IntVar(value=-1)
        self.id_current_frame = -1
        select_frame.add_radiobutton(label="0 - Страница приветствия", value=0, variable=self.current_radio,
                                     command=lambda: self.show_frame(0))
        select_frame.add_radiobutton(label="1 - Страница сети", value=1, variable=self.current_radio,
                                     command=lambda: self.show_frame(1))
        select_frame.add_radiobutton(label="2 - Страница создания новой сети", value=2, variable=self.current_radio,
                                     command=lambda: self.show_frame(2))
        select_frame.add_radiobutton(label="3 - Страница создания новых примеров", value=3, variable=self.current_radio,
                                     command=lambda: self.show_frame(3))
        menubar.add_cascade(label="Выбрать страницу", menu=select_frame)

        tk.Tk.config(self, menu=menubar)

        self.frames = []

        for F in (StartPage, PageBiNet, PageNewNet, PageNewExamples):  # Место подключения фреймов
            frame = F(container, self)
            self.frames.append(frame)
            frame.grid(row=0, column=0, sticky="nsew")  # наслаиваем друг на друга фреймы

        self.show_frame(0)

    def show_frame(self, id_frame: int):
        if self.id_current_frame != id_frame:
            self.frames[id_frame].tkraise()
            self.id_current_frame = id_frame
            self.current_radio.set(id_frame)

    def set_examples(self, examples):
        self.show_frame(1)
        self.frames[1].set_examples(examples)

    def set_binet(self, net):
        self.show_frame(1)
        self.frames[1].set_binet(net)
