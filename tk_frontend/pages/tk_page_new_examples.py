import tkinter as tk
from tkinter import ttk
import tkinter.messagebox as mb

from typing import Any, Dict, Union, List
import numpy as np
import hashlib

from PIL import Image, ImageTk

import random


def get_rnd_blood_hand(x: int, y: int, p: float) -> np.ndarray:
    mas = np.zeros((y, x), dtype=np.bool_)
    mas[0] = np.random.choice([True, False], x, p=[p, 1 - p])
    for i in range(mas.shape[0] - 1):
        for j in range(mas.shape[1]):
            if mas[i][j]:
                if 0 < j < mas.shape[1] - 1 and random.randint(0, 20) == 0:
                    mas[i + 1][j - 1] = True
                    mas[i + 1][j + 1] = True
                else:
                    offset = np.random.choice([-1, 0, 1], None, p=[0.3, 0.4, 0.3])
                    position = j + offset
                    if position >= mas.shape[1] or position < 0:
                        continue
                    else:
                        mas[i + 1][position] = True
    return mas


def number_to_bin_np(number: int, base: int) -> np.ndarray:
    string = bin(number)[2:]
    r_string = string[::-1]
    len_str = len(r_string)
    bool_list = []
    for i in range(base):
        if i < len_str:
            bool_list.append(r_string[i] == '1')
        else:
            bool_list.append(False)
    bool_list.reverse()
    return np.array(bool_list)


def access_bit(data, num):
    base = int(num // 8)
    shift = int(num % 8)
    return (data[base] & (1 << shift)) >> shift


def text_to_md5(text) -> List[int]:
    data = hashlib.md5(text.encode()).digest()
    return [access_bit(data, i) for i in range(len(data) * 8)]


class PageNewExamples(tk.Frame):
    controller: Any
    examples: Dict[str, Union[int, List[np.ndarray]]] = {
        "type": -1,
        "array": [],
        "out": []
    }

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        label = tk.Label(self, text="Страница создания новых примеров")
        label.pack(pady=5)

        group_control = tk.LabelFrame(self, text="Установите")
        group_control.pack(pady=5, padx=5, expand=True)

        # создание
        label = tk.Label(group_control, text="тип")
        label.grid(row=0, column=0, padx=10, pady=10)
        combo_example = ttk.Combobox(group_control, state="readonly", values=[
            "0 - создать слова (md5)",
            "1 - создать вены (20x20)",
            "2 - загрузить лица (peoples.npy)",
            "3 - загрузить реальные вены",
            "4 - 'солянка'"
        ], width=27)
        combo_example.grid(row=0, column=1, padx=10, pady=10)
        combo_example.current(1)

        label = tk.Label(group_control, text="количество уникальных примеров\n(для слов и вен)")
        label.grid(row=1, column=0, padx=10, pady=10)
        scale_count = tk.Scale(group_control, from_=1, to=100, orient=tk.HORIZONTAL, length=200)
        scale_count.grid(row=1, column=1, padx=10, pady=10)
        scale_count.set(15)

        label = tk.Label(group_control, text="функция разнообразия примеров\n(для вен и лиц)")
        label.grid(row=2, column=0, padx=10, pady=10)
        combo_function = ttk.Combobox(group_control, state="readonly", values=[
            "0 - шум",
            "1 - 90-градусные повороты"
        ], width=27)
        combo_function.grid(row=2, column=1, padx=10, pady=10)
        combo_function.current(0)

        label = tk.Label(group_control, text="количетсво разбиений примеров функцией\n(для вен, лиц и солянки)")
        label.grid(row=3, column=0, padx=10, pady=10)
        scale_sub = tk.Scale(group_control, from_=1, to=50, orient=tk.HORIZONTAL, length=200)
        scale_sub.grid(row=3, column=1, padx=10, pady=10)
        scale_sub.set(10)

        label = tk.Label(group_control, text="длина идентификатора")
        label.grid(row=4, column=0, padx=10, pady=10)
        scale_out = tk.Scale(group_control, from_=1, to=16, orient=tk.HORIZONTAL, length=200)
        scale_out.grid(row=4, column=1, padx=10, pady=10)
        scale_out.set(4)

        button_generate = ttk.Button(group_control, text="Сгенерировать примеры",
                                     command=lambda: self.generate_examples(combo_example.current(),
                                                                            combo_function.current(),
                                                                            int(scale_count.get()),
                                                                            int(scale_sub.get()),
                                                                            int(scale_out.get())
                                                                            )
                                     )
        button_generate.grid(row=5, column=0, columnspan=2, padx=10, pady=10)

        button_examples_binet = ttk.Button(group_control, text="Установить примеры",
                                           command=lambda: self.set_examples_binet())
        button_examples_binet.grid(row=6, column=0, columnspan=2, padx=10, pady=10)

        # просмотр
        label_1 = ttk.Label(group_control, text="отображаемый пример:")
        label_1.grid(row=7, column=0, padx=10, pady=10)

        self.scale_view = tk.Scale(group_control, from_=-1, to=-1, orient=tk.HORIZONTAL, length=200,
                                   command=lambda position: self.view(int(position)))
        self.scale_view.set(-1)
        self.scale_view.grid(row=7, column=1, padx=10, pady=10)

        self.label_example = ttk.Label(group_control, text="")
        self.label_example.grid(row=8, column=0, columnspan=2, padx=10, pady=10)

        self.label_out = ttk.Label(group_control, text="")
        self.label_out.grid(row=9, column=0, columnspan=2, padx=10, pady=10)

    def view(self, position):
        if position == -1:
            self.label_example.configure(text='', image=None)
            self.label_example.image = None
            self.label_out.configure(text='')
        else:
            if self.examples["type"] == 1 or self.examples["type"] == 2:
                data = self.examples["array"][position]
                data_img = np.array(data * 255, dtype=np.uint8)
                img_tk_new = ImageTk.PhotoImage(Image.fromarray(data_img, "L"))
                self.label_example.configure(image=img_tk_new)
                self.label_example.image = img_tk_new
                self.label_out.configure(text=str(self.examples["out"][position] * 1))
            elif self.examples["type"] == 0:
                self.label_example.configure(text=str(self.examples["array"][position] * 1))
                self.label_out.configure(text=str(self.examples["out"][position] * 1))
            else:
                self.label_example.configure(text='', image=None)
                self.label_example.image = None
                self.label_out.configure(text='')

    def generate_examples(self, choose, func, count, sub, id_len):
        if (0 <= choose <= 4) and (0 <= func <= 1):
            data_arr = []
            out_arr = []
            self.examples["type"] = choose
            if choose == 1:
                for i in range(count):
                    data = get_rnd_blood_hand(20, 20, 0.2)
                    for j in range(sub):
                        out_arr.append(number_to_bin_np(i + 1, id_len))
                        if j == 0:
                            data_arr.append(data)
                        else:
                            if func == 1:
                                data_arr.append(np.rot90(data, k=j))
                            elif func == 0:
                                data_arr.append(data ^ np.random.choice([True, False], data.shape, p=[0.01, 0.99]))
            elif choose == 0:
                for i in range(count):
                    test_str = str(random.randint(1, 126))
                    data0 = text_to_md5(test_str)
                    data = np.array(data0, dtype=np.bool_)
                    data_arr.append(data)
                    out_arr.append(number_to_bin_np(int(test_str), id_len))
            elif choose == 2:
                save_arr = np.load("/home/dura/my/surinet/binarise/peoples.npy")
                for i in range(save_arr.shape[0]):
                    data = save_arr[i]
                    for j in range(sub):
                        out_arr.append(number_to_bin_np(i + 1, id_len))
                        if j == 0:
                            data_arr.append(data)
                        else:
                            if func == 1:
                                data_arr.append(np.rot90(data, k=j))
                            elif func == 0:
                                data_arr.append(data ^ np.random.choice([True, False], data.shape, p=[0.01, 0.99]))
            elif choose == 3:
                self.examples["type"] = 2
                save_arr = np.load("/home/dura/my/surinet/binarise/save_arr2.npy")
                for i in range(save_arr.shape[0]):
                    if i >= count:
                        break
                    data = save_arr[i]
                    for j in range(sub):
                        out_arr.append(number_to_bin_np(i + 1, id_len))
                        if j == 0:
                            data_arr.append(data)
                        else:
                            if func == 1:
                                data_arr.append(np.rot90(data, k=j))
                            elif func == 0:
                                data_arr.append(data ^ np.random.choice([True, False], data.shape, p=[0.01, 0.99]))
            elif choose == 4:
                self.examples["type"] = 2
                save_arr_hand = np.load("/home/dura/my/surinet/binarise/save_arr2.npy")[:7]
                save_arr_peoples = np.load("/home/dura/my/surinet/binarise/peoples2.npy")
                save_arr_generate = np.array([get_rnd_blood_hand(93, 69, 0.2) for ii in range(7)])
                save_arr = np.concatenate((save_arr_hand, save_arr_peoples, save_arr_generate), axis=0)
                for i in range(save_arr.shape[0]):
                    data = save_arr[i]
                    for j in range(sub):
                        out_arr.append(number_to_bin_np(i + 1, id_len))
                        if j == 0:
                            data_arr.append(data)
                        else:
                            if func == 1:
                                data_arr.append(np.rot90(data, k=j))
                            elif func == 0:
                                data_arr.append(data ^ np.random.choice([True, False], data.shape, p=[0.01, 0.99]))
            self.examples["array"] = data_arr
            self.examples["out"] = out_arr
            self.scale_view.set(-1)
            self.scale_view.configure(to=len(self.examples["array"]) - 1)
            mb.showinfo("Выполнено", "Примеры успешно сгенерированы")
        else:
            mb.showerror("Ошибка", "Выбор {} {} не реализован!".format(choose, func))

    def set_examples_binet(self):
        if self.examples["type"] == -1:
            mb.showerror("Ошибка", "примеры не сгенерированы!")
        else:
            self.controller.set_examples(self.examples)
