import random

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import tkinter as tk
from tkinter import ttk
import tkinter.messagebox as mb

import numpy as np
from typing import Any, Dict, Union, List
from copy import deepcopy

from PIL import Image, ImageTk

import os
import sys

sys.path.append(os.path.abspath("../../backend"))
from backend.binet import BiNet


class PageBiNet(tk.Frame):
    controller: Any

    fig: Figure
    ax: Any
    canvas: FigureCanvasTkAgg
    toolbar: NavigationToolbar2Tk

    binet: BiNet = None
    examples: Dict[str, Union[int, List[np.ndarray]]] = {
        "type": -1,
        "array": [],
        "out": []
    }

    set_hash = False
    in_hash: List[np.ndarray]
    out_hash: List[np.ndarray]
    in_hash_run: List[np.ndarray]
    out_hash_run: List[np.ndarray]
    in_hash_check: List[np.ndarray]
    out_hash_check: List[np.ndarray]

    memory_now = {
        "position": -1,

        "in": np.array([]),
        "out": np.array([]),
        "want": np.array([]),

        "error_avg": [],
        "error_max": [],
        "error_future": []
    }

    def go_dump_select(self, event):
        self.view_up_graph(int(self.scale_up_graph.get()), self.combo_dump.current())

    def go_graph_other(self, event):
        self.view_other(self.combo_other.current())

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        label = tk.Label(self, text="Страница сети")
        label.pack(pady=5)

        group_control = tk.LabelFrame(self, text="Управление")
        group_control.pack(pady=5, padx=5, side=tk.RIGHT, fill=tk.BOTH)

        # УПРАВЛЕНИЕ

        # текущие примеры
        label_11 = ttk.Label(group_control, text="id примеров:")
        label_11.grid(row=0, column=0, padx=10, pady=10)

        self.label_id_examples = ttk.Label(group_control, text="-")
        self.label_id_examples.grid(row=0, column=1, padx=10, pady=10)

        # текущая сеть
        label_12 = ttk.Label(group_control, text="id сети:")
        label_12.grid(row=1, column=0, padx=10, pady=10)

        self.label_id_net = ttk.Label(group_control, text="-")
        self.label_id_net.grid(row=1, column=1, padx=10, pady=10)

        # создать примеры
        button_new_example = ttk.Button(group_control, text="Создать примеры",
                                        command=lambda: self.controller.show_frame(3))
        button_new_example.grid(row=2, column=0, padx=10, pady=10)

        button_load_example = ttk.Button(group_control, text="Загрузить примеры",
                                         command=lambda: mb.showerror("Ошибка", "Не реализовано!"))
        button_load_example.grid(row=2, column=1, padx=10, pady=10)

        # создать сеть
        button_new_net = ttk.Button(group_control, text="Создать сеть",
                                    command=lambda: self.controller.show_frame(2))
        button_new_net.grid(row=3, column=0, padx=10, pady=10)

        button_load_net = ttk.Button(group_control, text="Загрузить сеть",
                                     command=lambda: mb.showerror("Ошибка", "Не реализовано!"))
        button_load_net.grid(row=3, column=1, padx=10, pady=10)

        # разделитель
        ttk.Separator(group_control).grid(row=4, column=0, columnspan=2, pady=10, sticky=(tk.W, tk.E))

        # верхний график (дамп сети) выбор типа отображения
        label_4 = ttk.Label(group_control, text="верхний график:")
        label_4.grid(row=5, column=0, padx=10, pady=10)

        self.combo_dump = ttk.Combobox(group_control, state="readonly", values=[
            "0 - кластеры",
            "1 - антикластеры"])
        self.combo_dump.grid(row=5, column=1, padx=10, pady=10)
        self.combo_dump.current(0)
        self.combo_dump.bind("<<ComboboxSelected>>", self.go_dump_select)

        # верхний график (дамп сети) выбор номера
        label_4 = ttk.Label(group_control, text="номер отображения\nдля верхнего графика:")
        label_4.grid(row=6, column=0, padx=10, pady=10)

        self.scale_up_graph = tk.Scale(group_control, from_=-1, to=-1, orient=tk.HORIZONTAL, length=200,
                                       command=lambda position: self.view_up_graph(int(position),
                                                                                   self.combo_dump.current()))
        self.scale_up_graph.set(-1)
        self.scale_up_graph.grid(row=6, column=1, padx=10, pady=10)

        # типы остальных графиков
        label_4 = ttk.Label(group_control, text="остальные графики:")
        label_4.grid(row=7, column=0, padx=10, pady=10)

        self.combo_other = ttk.Combobox(group_control, state="readonly", values=[
            "0 - дамп работы",
            "1 - график ошибок"])
        self.combo_other.grid(row=7, column=1, padx=10, pady=10)
        self.combo_other.current(0)
        self.combo_other.bind("<<ComboboxSelected>>", self.go_graph_other)

        # запуск сети
        combo_run = ttk.Combobox(group_control, state="readonly", values=[
            "0 - forward отображаемого",
            "1 - del_add этап обучения",
            "2 - weight этап обучения",
            "3 - cut этап обучения",
            "4 - check_bad этап обучения",
            "5 - threshold этап обучения",
            "6 - 1 эпоха",
            "7 - 10 эпох"
        ])
        combo_run.grid(row=8, column=0, padx=10, pady=10)
        combo_run.current(0)

        button_run = ttk.Button(group_control, text="Выполнить", command=lambda: self.click_run(combo_run.current()))
        button_run.grid(row=8, column=1, padx=10, pady=10)

        # обрабатываемый пример
        label_3 = ttk.Label(group_control, text="обрабатываемый пример:")
        label_3.grid(row=9, column=0, padx=10, pady=10)

        self.label_run = ttk.Label(group_control, text="-")
        self.label_run.grid(row=9, column=1, padx=10, pady=10)

        # отображаемый пример
        label_1 = ttk.Label(group_control, text="отображаемый пример:")
        label_1.grid(row=10, column=0, padx=10, pady=10)

        # из tk стиль для Scale подразумевает подпись текущего номера
        self.scale_view = tk.Scale(group_control, from_=-1, to=-1, orient=tk.HORIZONTAL, length=200,
                                   command=lambda position: self.view_examples(int(position)))
        self.scale_view.set(-1)
        self.scale_view.grid(row=10, column=1, padx=10, pady=10)

        # ОТОБРАЖЕНИЕ ПРИМЕРОВ
        label_about = ttk.Label(group_control, text="ПРИМЕР:")
        label_about.grid(row=11, column=0, columnspan=2, pady=10)
        self.label_example = ttk.Label(group_control, text="")
        self.label_example.grid(row=12, column=0, columnspan=2, pady=10)

        self.label_out = ttk.Label(group_control, text="")
        self.label_out.grid(row=13, column=0, columnspan=2, pady=10)

        # ГРАФИКИ
        self.fig, self.ax = plt.subplots(4)
        self.fig.tight_layout()

        self.canvas = FigureCanvasTkAgg(self.fig, self)
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=True)
        self.toolbar = NavigationToolbar2Tk(self.canvas, self)  # внутри встроен pack()
        self.toolbar.update()

    # ПОШЛИ ФУНКЦИИ

    def set_binet(self, net: BiNet):
        answer = True
        if self.binet is not None:
            answer = mb.askyesno("Вопрос", "перезаписать сеть?\n(закрыть тоже значит 'нет')")
        if answer:
            self.memory_now["error_avg"] = []
            self.memory_now["error_max"] = []
            self.memory_now["error_future"] = []
            self.set_hash = False
            self.binet = deepcopy(net)
            self.label_id_net.configure(text=id(self.binet))
            # важен порядок
            self.scale_up_graph.set(-1)
            self.combo_dump.current(0)
            self.scale_up_graph.configure(to=len(self.binet.layer_neuron) - 1)
            self.memory_now["position"] = -1
            self.view_other(0)
            self.label_run.configure(text="-")

    def set_examples(self, examples):
        answer = True
        if self.examples["type"] != -1:
            answer = mb.askyesno("Вопрос", "перезаписать примеры?\n(закрыть тоже значит 'нет')")
        if answer:
            self.memory_now["error_avg"] = []
            self.memory_now["error_max"] = []
            self.memory_now["error_future"] = []
            self.set_hash = False
            self.examples = deepcopy(examples)
            self.label_id_examples.configure(text=id(self.examples))
            self.scale_view.set(-1)
            self.scale_view.configure(to=len(self.examples["array"]) - 1)
            self.memory_now["position"] = -1
            self.view_other(0)
            self.label_run.configure(text="-")

    def view_examples(self, position):
        if position == -1:
            self.label_example.configure(text='', image=None)
            self.label_example.image = None
            self.label_out.configure(text='')
        else:
            if self.examples["type"] == 1 or self.examples["type"] == 2:
                data = self.examples["array"][position]
                data_img = np.array(data * 255, dtype=np.uint8)
                img_tk_new = ImageTk.PhotoImage(Image.fromarray(data_img, "L"))
                self.label_example.configure(image=img_tk_new)
                self.label_example.image = img_tk_new
                self.label_out.configure(text=str(self.examples["out"][position] * 1))
            elif self.examples["type"] == 0:
                self.label_example.configure(text=str(self.examples["array"][position] * 1))
                self.label_out.configure(text=str(self.examples["out"][position] * 1))
            else:
                self.label_example.configure(text='', image=None)
                self.label_example.image = None
                self.label_out.configure(text='')

    def view_up_graph(self, position, choice):
        self.ax[0].clear()
        if position != -1:
            self.ax[0].set_title("слепок")
            arr0, arr0_4, arr4_7, arr7_1 = [], [], [], []
            clusters, weights = None, None
            if choice == 0:
                clusters = self.binet.dump_neuron_cluster(position)
                weights = self.binet.dump_cluster_weight(position)
            elif choice == 1:
                clusters = self.binet.dump_neuron_anti_cluster(position)
                weights = self.binet.dump_neuron_weight_anti_cluster(position)
            for h in range(clusters.shape[0]):
                for w in range(clusters.shape[1]):
                    if clusters[h, w]:
                        if weights[h, w] == 0:
                            arr0.append([w, h])
                        elif 0 < weights[h, w] < 0.4:
                            arr0_4.append([w, h])
                        elif 0.4 < weights[h, w] < 0.7:
                            arr4_7.append([w, h])
                        else:
                            arr7_1.append([w, h])
            if len(arr0) > 0:
                self.ax[0].scatter(np.array(arr0)[:, 0], np.array(arr0)[:, 1], s=1, marker='o', c="black")
            if len(arr0_4) > 0:
                self.ax[0].scatter(np.array(arr0_4)[:, 0], np.array(arr0_4)[:, 1], s=1, marker='s', c="blue")
            if len(arr4_7) > 0:
                self.ax[0].scatter(np.array(arr4_7)[:, 0], np.array(arr4_7)[:, 1], s=1, marker='s', c="green")
            if len(arr7_1) > 0:
                self.ax[0].scatter(np.array(arr7_1)[:, 0], np.array(arr7_1)[:, 1], s=1, marker='s', c="red")
        self.canvas.draw()
        self.toolbar.update()

    def view_other(self, choice):
        self.ax[1].clear()
        self.ax[2].clear()
        self.ax[3].clear()
        if self.memory_now["position"] != -1:
            if choice == 0:
                self.ax[1].bar(np.arange(len(self.memory_now["in"])), self.memory_now["in"])
                self.ax[1].set_title("вход")
                #
                self.ax[2].bar(np.arange(len(self.memory_now["out"])), self.memory_now["out"])
                self.ax[2].set_title("выход")
                #
                self.ax[3].bar(np.arange(len(self.memory_now["want"])), self.memory_now["want"])
                self.ax[3].set_title("ожидаемый выход")
            elif choice == 1:
                self.ax[1].plot(np.arange(len(self.memory_now["error_avg"])), self.memory_now["error_avg"])
                self.ax[1].set_title("средняя ошибка, %")
                #
                self.ax[2].plot(np.arange(len(self.memory_now["error_max"])), self.memory_now["error_max"])
                self.ax[2].set_title("максимальная ошибка, %")
                #
                self.ax[3].plot(np.arange(len(self.memory_now["error_future"])), self.memory_now["error_future"])
                self.ax[3].set_title("средняя ошибка предсказания, %")
            else:
                mb.showerror("Невозможно", "view_other")
        self.canvas.draw()
        self.toolbar.update()

    def one_epoch(self):
        self.binet.lesson_del_add(self.in_hash_run, self.out_hash_run)
        self.binet.lesson_weight(self.in_hash_run, self.out_hash_run)
        self.binet.lesson_cut()
        self.binet.lesson_threshold(self.in_hash_run, self.out_hash_run)
        self.binet.lesson_del_add(self.in_hash_run, self.out_hash_run)
        self.binet.lesson_check_bad(self.in_hash_run, self.out_hash_run)
        self.binet.lesson_del_add(self.in_hash_run, self.out_hash_run)
        self.binet.lesson_threshold(self.in_hash_run, self.out_hash_run)

    def set_hashes(self):
        # устанавливаем если не установлены хеши (10% на предсказание)
        if not self.set_hash:
            self.in_hash, self.out_hash = [], []
            self.in_hash_run, self.out_hash_run = [], []
            self.in_hash_check, self.out_hash_check = [], []
            for i in range(len(self.examples["array"])):
                if random.randint(0, 10) == 0 or len(self.out_hash_check) == 0:  # check
                    if self.examples["type"] == 0:
                        in_hash = self.binet.out_hash_1d_func(self.examples["array"][i], self.binet.size_in_vector)
                        self.in_hash.append(in_hash)
                        self.in_hash_check.append(in_hash)
                    else:
                        in_hash = self.binet.hash_2d_func(self.examples["array"][i], self.binet.size_in_vector)
                        self.in_hash.append(in_hash)
                        self.in_hash_check.append(in_hash)
                    out_hash = self.binet.out_hash_1d_func(self.examples["out"][i], self.binet.count_neuron)
                    self.out_hash.append(out_hash)
                    self.out_hash_check.append(out_hash)
                else:
                    if self.examples["type"] == 0:
                        in_hash = self.binet.out_hash_1d_func(self.examples["array"][i], self.binet.size_in_vector)
                        self.in_hash.append(in_hash)
                        self.in_hash_run.append(in_hash)
                    else:
                        in_hash = self.binet.hash_2d_func(self.examples["array"][i], self.binet.size_in_vector)
                        self.in_hash.append(in_hash)
                        self.in_hash_run.append(in_hash)
                    out_hash = self.binet.out_hash_1d_func(self.examples["out"][i], self.binet.count_neuron)
                    self.out_hash.append(out_hash)
                    self.out_hash_run.append(out_hash)
            self.set_hash = True
            mb.showinfo("Информация", "Примеров {}: {}-{} ({}%)".format(
                len(self.in_hash),
                len(self.in_hash_run),
                len(self.in_hash_check),
                round(len(self.in_hash_check) / len(self.in_hash) * 100, 1)
            ))

    def click_run(self, choice):
        if self.binet is not None and self.examples["type"] != -1:
            self.set_hashes()
            # выираем choice
            # "0 - forward отображаемого",
            # "1 - del_add этап обучения",
            # "2 - weight этап обучения",
            # "3 - cut этап обучения",
            # "4 - check_bad этап обучения",
            # "5 - threshold этап обучения",
            # "6 - 1 эпоха"
            # "7 - 10 эпох"
            if choice == 0:
                id_view = int(self.scale_view.get())
                if id_view == -1:
                    id_view = 0
                self.memory_now["position"] = id_view
            elif choice == 1:
                self.binet.lesson_del_add(self.in_hash_run, self.out_hash_run)
            elif choice == 2:
                self.binet.lesson_weight(self.in_hash_run, self.out_hash_run)
            elif choice == 3:
                self.binet.lesson_cut()
            elif choice == 4:
                self.binet.lesson_check_bad(self.in_hash_run, self.out_hash_run)
            elif choice == 5:
                self.binet.lesson_threshold(self.in_hash_run, self.out_hash_run)
            elif choice == 6:
                self.one_epoch()
            elif choice == 7:
                for ii in range(10):
                    self.one_epoch()
                    self.check_error()
                self.memory_now["position"] = random.randint(0, len(self.examples["array"]) - 1)

            if choice != 0 and choice != 7:
                self.check_error()
                self.memory_now["position"] = random.randint(0, len(self.examples["array"]) - 1)

            # ДЛЯ ВСЕХ
            position = self.memory_now["position"]
            self.memory_now["in"] = self.in_hash[position]
            self.memory_now["out"] = self.binet.forward(self.in_hash[position])
            self.memory_now["want"] = self.out_hash[position]
            # view
            self.label_run.configure(text=str(self.memory_now["position"]))
            self.view_up_graph(int(self.scale_up_graph.get()), self.combo_dump.current())
            self.view_other(self.combo_other.current())
        else:
            mb.showerror("Невозможно", "сеть и/или примеры не установлены")

    def check_error(self):
        errors = []
        for i in range(len(self.in_hash_run)):
            in_vector = self.in_hash_run[i]
            out_vector = self.binet.forward(in_vector)
            want_vector = self.out_hash_run[i]
            errors.append(
                (np.sum(out_vector ^ want_vector) / len(want_vector)) * 100
            )
        self.memory_now["error_avg"].append(np.average(errors))
        self.memory_now["error_max"].append(np.max(errors))
        errors = []
        for i in range(len(self.in_hash_check)):
            in_vector = self.in_hash_check[i]
            out_vector = self.binet.forward(in_vector)
            want_vector = self.out_hash_check[i]
            errors.append(
                (np.sum(out_vector ^ want_vector) / len(want_vector)) * 100
            )
        self.memory_now["error_future"].append(np.average(errors))
