import tkinter as tk
from tkinter import ttk


class StartPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Страница приветствия")
        label.pack(pady=5)

        label = tk.Label(self,
                         text="Добро пожаловать в программу, реализующую модель нейронной сети основанной на "
                              "концепции Редозубова"
                         )
        label.pack(pady=50)

        button = ttk.Button(self, text="Посетить страницу сети", command=lambda: controller.show_frame(1))
        button.pack(pady=10, padx=10)

        button4 = ttk.Button(self, text="Выйти", command=quit)
        button4.pack(pady=10)
