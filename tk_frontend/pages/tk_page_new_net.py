import tkinter as tk
from tkinter import ttk
from typing import Any

import os
import sys

sys.path.append(os.path.abspath("../../backend"))
from backend.binet import BiNet


class PageNewNet(tk.Frame):
    controller: Any

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        label = ttk.Label(self, text="Страница создания новой сети")
        label.pack(pady=5)

        group_control = tk.LabelFrame(self, text="Установите")
        group_control.pack(pady=5, padx=5, expand=True)

        label = ttk.Label(group_control, text="size_in_vector")
        label.grid(row=1, column=0, padx=10, pady=10)
        self.scale_size_in_vector = tk.Scale(group_control, from_=50, to=500, orient=tk.HORIZONTAL, length=200)
        self.scale_size_in_vector.grid(row=1, column=1, padx=10, pady=10)
        self.scale_size_in_vector.set(150)

        label = ttk.Label(group_control, text="count_cluster")
        label.grid(row=2, column=0, padx=10, pady=10)
        self.scale_count_cluster = tk.Scale(group_control, from_=1, to=100, orient=tk.HORIZONTAL, length=200)
        self.scale_count_cluster.grid(row=2, column=1, padx=10, pady=10)
        self.scale_count_cluster.set(10)

        label = ttk.Label(group_control, text="count_neuron")
        label.grid(row=3, column=0, padx=10, pady=10)
        self.scale_count_neuron = tk.Scale(group_control, from_=1, to=100, orient=tk.HORIZONTAL, length=200)
        self.scale_count_neuron.grid(row=3, column=1, padx=10, pady=10)
        self.scale_count_neuron.set(4)

        ttk.Separator(group_control, orient=tk.VERTICAL).grid(row=1, column=2, rowspan=4, pady=10, sticky=(tk.N, tk.S))

        label = ttk.Label(group_control, text="hash_window")
        label.grid(row=1, column=3, padx=10, pady=10)
        self.scale_hash_window = tk.Scale(group_control, from_=1, to=5, orient=tk.HORIZONTAL, length=200)
        self.scale_hash_window.grid(row=1, column=4, padx=10, pady=10)
        self.scale_hash_window.set(2)

        label = ttk.Label(group_control, text="hash_1d_count")
        label.grid(row=2, column=3, padx=10, pady=10)
        self.scale_hash_1d_count = tk.Scale(group_control, from_=1, to=100, orient=tk.HORIZONTAL, length=200)
        self.scale_hash_1d_count.grid(row=2, column=4, padx=10, pady=10)
        self.scale_hash_1d_count.set(5)

        label = ttk.Label(group_control, text="hash_2d_count")
        label.grid(row=3, column=3, padx=10, pady=10)
        self.scale_hash_2d_count = tk.Scale(group_control, from_=1, to=100, orient=tk.HORIZONTAL, length=200)
        self.scale_hash_2d_count.grid(row=3, column=4, padx=10, pady=10)
        self.scale_hash_2d_count.set(10)

        button = ttk.Button(group_control, text="Сгенерировать и установить",
                            command=lambda: self.click_button_set_binet())
        button.grid(row=5, column=0, padx=10, pady=10, columnspan=5)

    def click_button_set_binet(self):
        net = BiNet(
            int(self.scale_size_in_vector.get()),
            int(self.scale_count_cluster.get()),
            int(self.scale_count_neuron.get()),

            int(self.scale_hash_window.get()),
            int(self.scale_hash_1d_count.get()),
            int(self.scale_hash_2d_count.get())
        )
        self.controller.set_binet(net)
