import numpy as np
from typing import List, Union
import perfplot
import numba as nb

arr_target = np.array([0, 1, 0])


@nb.njit(fastmath=True)
def find_any_equal_nb(arr2: Union[np.ndarray, List[List[bool]]]) -> bool:
    for i in range(arr2.shape[0]):
        if np.array_equal(arr2[i], arr_target):
            return True
    return False


def find_any_equal(arr2: Union[np.ndarray, List[List[bool]]]) -> bool:
    for i in range(arr2.shape[0]):
        if np.array_equal(arr2[i], arr_target):
            return True
    return False


perfplot.show(
    setup=lambda n: np.random.randint(0, 2, (n, 3)),
    n_range=[2 ** k for k in range(15)],
    kernels=[find_any_equal,
             find_any_equal_nb
             ],
    labels=["Обычная функция", "Функция numba"],
    xlabel="Размер массива",
    time_unit="us",
    logx=True,
    logy=True,
)
